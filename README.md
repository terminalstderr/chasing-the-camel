# README #

This is a Unity Project that is a simple platformer video game that is an extension of a teaching project that I put together for Undergraduate students Fall term of 2016 at the University of Oregon. I have continued to put efforts into the project beyond the basic teaching project that I defined.

Original Project as defined by Ryan Leonard:

[https://www.cs.uoregon.edu/Classes/16F/cis441/unity/project_g_unity.pdf
](https://www.cs.uoregon.edu/Classes/16F/cis441/unity/project_g_unity.pdf)

# Playtest Demonstrations #

https://youtu.be/3V0tIID39Io

https://youtu.be/LBAiWFO88Rw

https://youtu.be/jGcB9gKYfFk

### Who do I talk to? ###

Contact Ryan Leonard with any quesitons!
Ryan.Leonard71@gmail.com