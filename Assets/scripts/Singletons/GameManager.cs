﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class GameManager : MonoBehaviour
//public class GameManager
{
    public Text ui_center;
    private string[] levels = { "level_1", "level_2", "level_3"};
    private string menu_level = "menu";
    private string victory_level = "victory";
    private string exit_credits = "Chasing the Camel \nRyan Leonard \nHaley Whitman";
    private string intro_credits = "Chasing the Camel";

    private bool mutex_lock;

    public static GameManager instance = null;     //Allows other scripts to call functions from SoundManager.             

    void Awake()
    {
        clear_notice_ui();
        string active_level = SceneManager.GetActiveScene().name;

        if (active_level == victory_level)
        {
            update_notice_ui(exit_credits);
        }
        else if (active_level == menu_level)
        {
            update_notice_ui(intro_credits);
        }
        //Check if there is already an instance of SoundManager
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
        //DontDestroyOnLoad(gameObject);

    }

    public void level_finished()
    {
        if (!mutex_lock)
        {
            string active_level = SceneManager.GetActiveScene().name;
            // We have a special-case behavior if we are on the 'menu' level
            if (active_level == menu_level)
            {
                Debug.Log("Starting the game");
                Invoke("start_game", 3);
                update_notice_ready_set_go();
            }
            else {
                Debug.Log("Finished a level! Congratulations!");
                slow_motion();
                Invoke("resume_game", 0.2f);
                Invoke("next_level", 0.2f);
                update_notice_ui("Level Complete!");
            }
            //mutex_lock = true;
        }
    }

    private void restart_game()
    {
        Debug.Log("Loading menu: " + menu_level);
        SceneManager.LoadSceneAsync(menu_level);
    }

    private void start_game()
    {
        Debug.Log("Loading first level: " + levels[0]);
        SceneManager.LoadSceneAsync(levels[0]);
    }

    private void next_level()
    {
        string active_level = SceneManager.GetActiveScene().name;
        for (int i = 0; i < levels.Length; i++)
        {
            if (active_level == levels[i])
            {
                if (i + 1 < levels.Length)
                {
                    Debug.Log("Loading next level: " + levels[i + 1]);
                    SceneManager.LoadSceneAsync(levels[i + 1]);
                    return;
                }
                else
                {
                    Debug.Log("Finished all the main levels! Going to load 'victory lap' scene: " + victory_level);
                    SceneManager.LoadSceneAsync(victory_level);
                    return;
                }
            }
        }
        // If we are playing a that is not in our 'levels' then we just reload the active level
        Debug.Log("Current scene not handled by our game manager: " + active_level);
        SceneManager.LoadSceneAsync(active_level);
    }

    private void slow_motion()
    {
        Time.timeScale = 0.33f;
    }

    private void pause_game()
    {
        Time.timeScale = 0.0F;
    }

    private void resume_game()
    {
        Time.timeScale = 1.0F;
    }

    private void update_notice_ready_set_go()
    {
        Invoke("ui_ready", 0.0F);
        Invoke("ui_set", 0.75F);
        Invoke("ui_go", 1.5F);
    }

    private void ui_ready() { update_notice_ui("Ready?"); }
    private void ui_set() { update_notice_ui("Set..."); }
    private void ui_go() { update_notice_ui("Go!"); }

    private void update_notice_ui(string str)
    {
        if (ui_center)
            ui_center.text = str;
    }

    private void clear_notice_ui()
    {
        if (ui_center)
            ui_center.text = "";
    }

}
