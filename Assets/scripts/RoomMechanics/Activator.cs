﻿using UnityEngine;
using System.Collections;
using System;

public class Activator : MonoBehaviour {
    public GameObject activator;
    public ActivatedBehaviour behaviour;
    private Animator m_anim;
    public AudioClip activated_clip;

    void Awake()
    {
        m_anim = GetComponent<Animator>();
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag(activator.tag))
            _Activate();
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag(activator.tag))
            _Activate();
    }

	void OnTriggerExit2D(Collider2D other)
	{
		if (other.gameObject.CompareTag(activator.tag))
            _Deactivate();
	}

    public void _Activate()
    {
        if (m_anim)
        {
            m_anim.SetTrigger("pressed");
            SoundManager.instance.PlaySingle(activated_clip);
        }
        behaviour.Activate();
    }

    public void _Deactivate()
	{
        if (m_anim)
        {
            m_anim.ResetTrigger("pressed");
            SoundManager.instance.PlaySingle(activated_clip);
        }
        behaviour.Deactivate();
    }
}
