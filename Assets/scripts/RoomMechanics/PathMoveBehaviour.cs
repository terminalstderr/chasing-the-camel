﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using System;

struct Waypoint
{
    public Vector3 pos;
    public float delay;

    public Waypoint(Vector3 pos, float delay) : this()
    {
        this.pos = pos;
        this.delay = delay;
    }
}

public class PathMoveBehaviour : ActivatedBehaviour {
    public GameObject path;
    public bool manual_delays;
    public float[] delays;
    public String waypoint_tag = "waypoint";
    private bool activated = false;

    public override void Activate()
    {
        if (!activated)
        {
            StartFollowPath();
            activated = true;
        }
    }

    private void StartFollowPath()
    {
        List<Waypoint> waypoints = new List<Waypoint>();
        // Build a collection of waypoints
        // From our investigation, the order of the childobjects matches the order provided in the 
        // hierarchy view of the Path game object.
        int i = 0;
        Vector3 pos = gameObject.transform.position;
        Vector3 next_pos;
        foreach (Transform wp in path.transform)
        {
            if (wp.CompareTag(waypoint_tag))
            {
                if (!manual_delays)
                {
                    next_pos = wp.position;
                    float distance = (pos - next_pos).magnitude;
                    // TODO: Enable a 'units-per-second' input to this script
                    // We were going to divide by some constant, but turns out that one second per unit is pretty nice.
                    float delay = distance;
                    waypoints.Add(new Waypoint(wp.position, delay));
                    pos = next_pos;
                }
                else
                {
                    waypoints.Add(new Waypoint(wp.position, delays[i]));
                }
                i++;
            }
        }
        Debug.Log("Finished constructing Waypoints from path and delays");
        StartCoroutine(FollowPath(waypoints));
    }

    private IEnumerator FollowPath(List<Waypoint> waypoints)
    {
        foreach (Waypoint waypoint in waypoints)
        {
            yield return StartCoroutine(Utility.MoveOverSeconds(gameObject, waypoint.pos, waypoint.delay));
        }
    }

}
