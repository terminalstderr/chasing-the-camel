﻿using UnityEngine;
using System.Collections;
using System;

public class RelativeMoveBehaviour : ActivatedBehaviour {
    public float seconds = 2.0F;
    public Vector2 movement = Vector2.zero;
    private bool activated = false;

    private void move_object(GameObject obj, Vector2 movement, float delay_seconds)
    {
        Vector2 end = new Vector2(obj.transform.position.x + movement.x,  obj.transform.position.y + movement.y);
        StartCoroutine(Utility.MoveOverSeconds(obj, end, delay_seconds));
    }

    public override void Activate()
    {
        if (!activated)
        {
            move_object(gameObject, movement, seconds);
            activated = true;
        }
    }
}
