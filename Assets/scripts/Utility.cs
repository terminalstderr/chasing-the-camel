﻿using UnityEngine;
using System.Collections;
using System;

public class Utility {
    internal static IEnumerator TransformOverSeconds(GameObject obj, Vector3 end_position, Vector3 end_scale,
        Quaternion end_rotation, float seconds)
    {
        float elapsedTime = 0;
        Rigidbody2D rb = obj.GetComponent<Rigidbody2D>();
        Transform trans = obj.transform;
        Vector3 startingScale = trans.localScale;
        Vector3 startingPos = trans.position;
        Quaternion startingRot = trans.rotation;
        while (elapsedTime < seconds)
        {
            //trans.position = Vector3.Lerp(startingPos, end_position, Mathf.SmoothStep(0, 1, elapsedTime / seconds));
            rb.MovePosition(Vector3.Lerp(startingPos, end_position, Mathf.SmoothStep(0, 1, elapsedTime / seconds)));
            trans.localScale = Vector3.Lerp(startingScale, end_scale, Mathf.SmoothStep(0, 1, elapsedTime / seconds));
            Quaternion.Lerp(startingRot, end_rotation, Mathf.SmoothStep(0, 1, elapsedTime / seconds));

            // Sync up with frame rate
            elapsedTime += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        //trans.position = Vector3.Lerp(startingPos, end_position, Mathf.SmoothStep(0, 1, 1));
        rb.MovePosition(Vector3.Lerp(startingPos, end_position, Mathf.SmoothStep(0, 1, 1)));
        trans.localScale = Vector3.Lerp(startingScale, end_scale, Mathf.SmoothStep(0, 1, 1));
        Quaternion.Lerp(startingRot, end_rotation, Mathf.SmoothStep(0, 1, 1));
    }

    public static IEnumerator MoveOverSeconds(GameObject obj, Vector3 end, float seconds)
    {
        Transform t = obj.transform;
        yield return TransformOverSeconds(obj, end, t.localScale, t.rotation, seconds);
    }

    public static IEnumerator ScaleOverSeconds(GameObject obj, Vector3 end, float seconds)
    {
        Transform t = obj.transform;
        yield return TransformOverSeconds(obj, t.position, end, t.rotation, seconds);
    }

    public static IEnumerator RotateOverSeconds(GameObject obj, Quaternion end, float seconds)
    {
        Transform t = obj.transform;
        yield return TransformOverSeconds(obj, t.position, t.localScale, end, seconds);
    }

    internal static IEnumerator TransformOverSeconds(GameObject obj, Transform end, float seconds)
    {
        yield return TransformOverSeconds(obj, end.position, end.localScale, end.rotation, seconds);
    }
}

public abstract class ActivatedBehaviour : MonoBehaviour
{
	//Child class must implement
    public abstract void Activate();

	//Child class may implement
	public virtual void Deactivate (){}
}
	
