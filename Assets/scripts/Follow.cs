﻿using UnityEngine;
using System.Collections;

public class Follow : MonoBehaviour {

    public GameObject toFollow;
    private Vector3 offset;
    public bool follow_x = true;
    public bool follow_y = true;
    public bool follow_z = true;

    // Use this for initialization
    void Start()
    {
        UpdateOffset();
    }

    public void UpdateOffset()
    {
        offset = transform.position - toFollow.transform.position;
    }

    public void FollowXY()
    {
        follow_x = true;
        follow_y = true;
        follow_z = false;
    }

    public void FollowOnlyX()
    {
        follow_x = true;
        follow_y = false;
        follow_z = false;
    }

    public void FollowOnlyY()
    {
        follow_x = false;
        follow_y = true;
        follow_z = false;
    }

    public void DontFollow()
    {
        follow_x = false;
        follow_y = false;
        follow_z = false;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        Vector3 diff = toFollow.transform.position + offset;
        float x = follow_x ? diff.x : transform.position.x;
        float y = follow_y ? diff.y : transform.position.y;
        float z = follow_z ? diff.z : transform.position.z;
        transform.position = new Vector3(x, y, z);
    }
}
