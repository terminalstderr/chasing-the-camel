﻿using UnityEngine;
using System.Collections;

public class PlatformEnemyController : MonoBehaviour {

    public float time_per_side = 2.0F;
    public GameObject platform;
    public bool clockwise = false;
    private Vector3 perimeter_scale = new Vector3(1.0f,1.0f,1.0f);

    void Start() {
        StartCoroutine(orbit_cube(platform));
    }

    private IEnumerator orbit_cube(GameObject cube)
    {
        Vector3 scale = cube.transform.localScale;
        Vector3 p = perimeter_scale;
        Vector3[] corners =
        {
            Vector3.Scale(scale / 2, new Vector3( p.x,   p.y, p.z)),
            Vector3.Scale(scale / 2, new Vector3(-p.x,   p.y, p.z)),
            Vector3.Scale(scale / 2, new Vector3(-p.x,  -p.y, p.z)),
            Vector3.Scale(scale / 2, new Vector3( p.x,  -p.y, p.z))
        };

        Vector3[] path =
        {
            transform.TransformPoint(corners[0]),
            transform.TransformPoint(corners[1]),
            transform.TransformPoint(corners[2]),
            transform.TransformPoint(corners[3])
        };

        if (clockwise)
        {
            path = MakePathClockwise(path);
        }

        //Now we just need to lerp our way between these 4 vertices
        int L = path.Length;
        int next_i;
        for (int i = 0; true; i = (i + 1) % L)
        {
            next_i = (i + 1) % L;
            gameObject.transform.position = path[i];
            yield return StartCoroutine(Utility.MoveOverSeconds(gameObject, path[next_i], time_per_side));
        }
    }

    private Vector3[] MakePathClockwise(Vector3[] path)
    {
        Vector3[] ret =
        {
            path[3],
            path[2],
            path[1],
            path[0]
        };
        return ret;
    }
}
