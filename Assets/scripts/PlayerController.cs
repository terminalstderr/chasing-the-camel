﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {
    public float double_tap_time = 0.03F;
    public float double_tap_boost = 1.5F;
    public float bump_force= 1000.0F;
    public string bumper_tag;
    public string pickup_tag;
    public string obstacle_tag;
    public string finish_tag;
    public GameObject live_box;
    public GameManagerOld gm;

    private Rigidbody2D m_rb;
    private Collider2D m_collider;
    private Animator m_animator;
    private float get_axis_horizontal = 0.0F;
    private bool facing_right = true;

    private DoubleTapDetector dtd_left;
    private DoubleTapDetector dtd_right;

	public bool hasKey = false;

    // TODO: Add a tiny delay after a player jumps in which they are _not_ grounded. This enables us to use 'getKey' instead of 'getKeyDown'
    // TODO: Use the collider based approach to isGrounded detection alongside the raytrace based approach.

    // Use this for initialization
    void Start () {
        m_rb = GetComponent<Rigidbody2D>();
        m_collider = GetComponent<Collider2D>();
        m_animator = GetComponent<Animator>();
        dtd_left = new DoubleTapDetector("a", double_tap_time);
        dtd_right = new DoubleTapDetector("d", double_tap_time);
    }

    // We use update to get inputs from the keyboard/player
    // this is the proper place to gather inputs to ensure that we don't drop any keystrokes which can happen in FixedUpdate
    void Update()
    {
    }

    void FixedUpdate ()
    {
        float movement = get_axis_horizontal;
        AnimatorUpdate(movement);

        /*
        if (dtd_left.detectedDoubleTap())
        {
            // TODO: Do we want a 'smooth' boost or a 'dash' boost? Smooth: use moveHorizontal, Dash: modify velocity directly
            m_rb.velocity = new Vector3(m_rb.velocity.x-double_tap_boost, m_rb.velocity.y, m_rb.velocity.z);
            //moveHorizontal *= double_tap_boost;
        }
        if (dtd_right.detectedDoubleTap())
        {
            // TODO: Do we want a 'smooth' boost or a 'dash' boost? Smooth: use moveHorizontal, Dash: modify velocity directly
            m_rb.velocity = new Vector3(m_rb.velocity.x + double_tap_boost, m_rb.velocity.y, m_rb.velocity.z);
            //moveHorizontal *= double_tap_boost;
        }
        */
    }

    void AnimatorUpdate(float movement)
    {
        // figure out if we have changed directions since last update
        if (movement > 0 && !facing_right ||
            movement < 0 && facing_right)
        {
            Flip();
        }
    }

    void Flip()
    {
        // Borrowed from: https://unity3d.com/learn/tutorials/topics/2d-game-creation/creating-basic-platformer-game
        facing_right = !facing_right;
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }

    void OnTriggerEnter2D(Collider2D other )
    {
        if (other.gameObject.CompareTag(pickup_tag))
        {
            other.gameObject.SetActive(false);
            gm.score++;
            Debug.Log("Player scored! " + gm.score);
        }
        if (other.gameObject.CompareTag(obstacle_tag))
        {
            // TODO: Make a Particle explosion effect
            if (!gm.mutex_lock)
            {
                game_over_effects();
                gm.game_over();
                Debug.Log("Player colided with obstacle... Game Over");
            }
        }
        // TODO: Disabled the bumper enemy since the 2D physics doesn't have the ability to 
        // "Add explosive force"
        //if (other.gameObject.CompareTag(bumper_tag))
        //{
        //    Vector3 enemy_position = other.gameObject.transform.parent.transform.position;
        //    m_rb.AddExplosionForce(bump_force,enemy_position, 30);
        //    Rigidbody2D o_rb = other.gameObject.GetComponentInParent<Rigidbody2D>();
        //    o_rb.AddExplosionForce(bump_force, transform.position, 30);
        //}
        if (other.gameObject.CompareTag(finish_tag))
        {
            if (!gm.mutex_lock)
            {
                gm.level_finished();
                Debug.Log("Player finished the level!");
            }
        }

    }

    void OnTriggerExit2D(Collider2D other)
    {
        //if (other.gameObject.CompareTag(live_box.tag))
        //{
            // TODO: Make a particle explosion onto screen or make the ball bounce in some way?
         //   game_over_effects();
          //  gm.game_over();
       // }
    }


    void game_over_effects()
    {
        gameObject.SetActive(false);
    }
}

class DoubleTapDetector
{
    private float last_tap = 0.0F;
    private float thresh;
    private string key;
    public DoubleTapDetector(string _key, float double_tap_time)
    {
        this.thresh = double_tap_time;
        this.key = _key;
    }

    public bool detectedDoubleTap()
    {
        bool ret = false;
        if (Input.GetKeyDown(key)) {
            // If the key was pressed within the threshold from the last time it 
            // was pressed, we have successfully detected a double tap 
            if (Time.time - last_tap < thresh)
            {
                Debug.Log("Double Tap detected for key " + key);
                ret = true;
            }
            // Whenever the key is pressed we want to update our 'last_tap' time
            last_tap = Time.time;
        }
        return ret;
    }
}
