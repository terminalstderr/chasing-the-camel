﻿ using UnityEngine;
 using System.Collections;
 
 // @NOTE the attached sprite's position should be "top left" or the children will not align properly
 // Strech out the image as you need in the sprite render, the following script will auto-correct it when rendered in the game
 [RequireComponent(typeof(SpriteRenderer))]

// NOTICE: The scale of the object should be divisable by size of the sprites used (all sprites should be same size) 
// NOTICE: The object used should be rectangular
public class RepeatSprite : MonoBehaviour
{
    public Sprite[] sprites;

    private SpriteRenderer sprite_renderer;

    void Awake()
    {
        // Get the sprite renderer
        sprite_renderer = GetComponent<SpriteRenderer>();
        Vector2 sprite_size = new Vector2(sprite_renderer.bounds.size.x / transform.localScale.x, sprite_renderer.bounds.size.y / transform.localScale.y);
        Sprite default_sprite = sprite_renderer.sprite;
        Color color = sprite_renderer.color;

        // Generate a child prefab of the sprite renderer
        GameObject childPrefab = new GameObject();
        SpriteRenderer childSprite = childPrefab.AddComponent<SpriteRenderer>();
        childPrefab.transform.position = transform.position;
        childSprite.sprite = default_sprite;
        childSprite.color = color;
        childSprite.sortingLayerID = sprite_renderer.sortingLayerID;

        // Loop through and spit out repeated tiles
        GameObject child;
        var ybound = Mathf.Round(sprite_renderer.bounds.size.y) / 2;
        var xbound = Mathf.Round(sprite_renderer.bounds.size.x) / 2;
        for (float y = -ybound; y < ybound; y++)
        {
            for (float x = -xbound; x < xbound; x++)
            {
                child = Instantiate(childPrefab) as GameObject;
                set_sprite(child);
                child.transform.position = transform.position + (new Vector3(x+(sprite_size.x/2), y+(sprite_size.y/2), 0));
                child.transform.parent = transform;
            }
        }
        // Set the parent last on the prefab to prevent transform displacement
        childPrefab.transform.parent = transform;
        childPrefab.SetActive(false);

        // TODO Why isn't this done at the begining?
        // Disable the currently existing sprite component since its now a repeated image
        sprite_renderer.enabled = false;
    }

    void set_sprite(GameObject obj)
    { 
        if (sprites.Length > 0)
        {
            var sr = obj.GetComponent<SpriteRenderer>() as SpriteRenderer;
            var index = Random.Range(0, sprites.Length);
            sr.sprite = sprites[index];
        }
    }

}
