﻿using UnityEngine;
using System.Collections;

public class Rotate : MonoBehaviour {
    public Vector3 rotate_amount = new Vector3(15,30,35);
    void Update()
    {
        transform.Rotate(rotate_amount * Time.deltaTime);
    }
}
