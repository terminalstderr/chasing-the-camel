﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
/**
This file is home to the Livebox Controller system which includes the ability for external actors
    to be able to update this livebox controller on the fly.
    The motivation for this is to have it so that when the livebox collides with some part of the scene, 
    the game will seamlessly be able to change the livebox travel mode, e.g. enter a stationary mode until the player
    presses some button, or enter a path following mode for some amount of time, or change from traveling with some x 
    velocity to traveling with some y velocity.
 */


public class LiveboxController : MonoBehaviour {
    private IEnumerator coroutine = null;
    private Follow follow_player;

    // TODO: Should this be a part of the LiveboxUpdater knobs? E.g. move this to LiveboxUpdater.cs
    public string waypoint_tag;

    public void UpdateMode(LiveboxControllerUpdate u)
    {
        follow_player = getFollowPlayer();
        switch (u.mode)
        {
            case LiveboxMode.Velocity:
                UpdateVelocity(u.direction * u.speed);
                break;
            case LiveboxMode.Path:
                UpdatePath(u.path, u.delays);
                break;
            case LiveboxMode.Follow:
                UpdateFollow();
                break;
            case LiveboxMode.Stationary:
            default: // Default behavior is stationary
                UpdateStationary();
                break;
        }
    }

    private void stop_coroutine()
    {
        if (coroutine != null)
        {
            StopCoroutine(coroutine);
        }
    }

    private void UpdateStationary()
    {
        // When we switch modes, we have to stop the current mode.
        stop_coroutine();
        follow_player.DontFollow();
    }

    private void UpdateFollow()
    {
        // When we switch modes, we have to stop the current mode.
        stop_coroutine();
        follow_player.FollowXY();
    }

    private void UpdateVelocity(Vector3 velocity)
    {
        // When using a velocity in the x-only or the y-only direction, we enable the opposite direction
        // to have free range of motion.  
        if (velocity.x == 0)
            follow_player.FollowOnlyX();
        else if (velocity.y == 0)
            follow_player.FollowOnlyY();

        // When we switch modes, we have to stop the current mode.
        stop_coroutine();
        coroutine = ConstantVelocity(velocity);
        StartCoroutine(coroutine);
    }



    // NOTICE: Want the delay to be about 4-units per second...
    // TODO: Could make it so that when the player game object collides with a triger, the path will be updated to take
    // an alternative route... hmmm...
    private void UpdatePath(GameObject path, float[] delays)
    {
        follow_player.DontFollow();
        List<Waypoint> waypoints = new List<Waypoint>();
        // Build a collection of waypoints
        // From our investigation, the order of the childobjects matches the order provided in the 
        // hierarchy view of the Path game object.
        int i = 0;
        foreach (Transform child in path.transform)
        {
            if (child.CompareTag(waypoint_tag))
            {
                waypoints.Add(new Waypoint(child.position, delays[i]));
                i++;
            }
        }
        Debug.Log("Finished constructing Waypoints from path and delays");

        // When we switch modes, we have to stop the current mode.
        if (coroutine != null)
        {
            StopCoroutine(coroutine);
        }
        coroutine = FollowPath(waypoints);
        StartCoroutine(coroutine);
    }

    private IEnumerator ConstantVelocity(Vector3 velocity)
    {
        while (true)
        {
            transform.position += velocity;
            yield return new WaitForEndOfFrame();
        }
    }

    private IEnumerator FollowPath(List<Waypoint> waypoints)
    {
        foreach (Waypoint waypoint in waypoints)
        {
            yield return StartCoroutine(Utility.MoveOverSeconds(gameObject, waypoint.pos, waypoint.delay));
        }
    }

    private Follow getFollowPlayer()
    {
        if (follow_player == null)
        {
            follow_player = GetComponent<Follow>();
        }
        // Whenever we get the Follow object, we will update the offset of the livebox following.
        // This keeps from having the livebox jerk or jitter back to the player's location
        // NOTE: There is a danger here that the offset could get messed up if for example the player somehow
        //      presses a button while too far to the left/right -- the solution is to have a checkpoint before
        //      using a velocity change such that the player is semi-forced to 'rest' while at the checkpoint.
        //      This is manifested by having a large platform with a 'continue' button on the far right of it. 
        follow_player.UpdateOffset();
        return follow_player;
    }
}

/// <summary>
/// There are 4 Livebox modes. The most useful is Velocity for our scroller game, but occasinoally we have
/// interactions in game where we would prefer to use a path following camera, an x-y free range following livebox,
/// or a completely stationary livebox.
/// </summary>
public enum LiveboxMode
{
    Velocity,
    Path,
    Stationary,
    Follow
}

public class LiveboxControllerUpdate
{
    public Vector3 direction;
    public float speed;
    public float[] delays;
    public GameObject path;
    public LiveboxMode mode;
}