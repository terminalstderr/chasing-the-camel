﻿using UnityEngine;
using System.Collections;

/// <summary>
/// LiveboxUpdater updates the LiveboxController at the occurance of some event. The events that we've
/// built in to trigger a LiveboxController update is either the Start event, a Trigger event with some other
/// object, or a Collision event with some other object.
/// 
/// Notice: It is a good practice to use the Player game object as the trigger/collision object.
/// 
/// Velocit Mode Settings:
///     direction: The direction, normally either the unit vector {1,0,0}, {0,-1,0}, or {0,1,0}
///     speed: The speed at which to travel in that direction, a good value is 0.03
/// 
/// Path Mode Settings:
///     delays: A list of delay times to use when traveling from one waypoint to the next. Notice that the first
///         delay time is associated with the time to travel to the first waypoint, e.g. it is not assumed that the
///         livebox begins at the first waypoint.
///     path: this is a special game object that contains children "Waypoint" game objects. The livebox controller 
///         will follow all of the waypoints in the order that they are declared in the Path game object.
/// </summary>
public class LiveboxUpdater : MonoBehaviour {
    public LiveboxController liveboxController;
    public LiveboxMode mode;
    [Header("")]
    public bool cinematic = false;
    public bool OnStart = false;
    public bool OnTrigger = false;
    public GameObject OnTriggerWith;
    public bool OnCollision = false;
    public GameObject OnCollisionWith;
    [Header("")]
    [Header("'Velocity' Mode Knobs")]
    public Vector3 direction = new Vector3(0.0F, 0.0F, 0.0F);
    public float speed = 1.0F;
    [Header("'Path' Mode Knobs")]
    public float[] delays;
    public GameObject path;

    void Start()
    {
        if (OnStart)
        { 
            updateLiveboxController();
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (OnTrigger && other.name == OnTriggerWith.name)
        {
            updateLiveboxController();
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        if (OnCollision && collision.gameObject.name == OnCollisionWith.name)
        {
            updateLiveboxController();
        }
    }

    private void updateLiveboxController()
    {
        if (this.enabled)
        {
            if (cinematic)
            {
                // If we are in cimenatic mode, disable the livebox (this could cause bugs
                liveboxController.GetComponentInParent<Collider>().enabled = false;
            }
            else
            {
                liveboxController.GetComponentInParent<Collider>().enabled = true;

            }
            LiveboxControllerUpdate update = new LiveboxControllerUpdate();
            update.direction = direction;
            update.speed = speed;
            update.delays = delays;
            update.path = path;
            update.mode = mode;
            liveboxController.UpdateMode(update);
            // We only want each livebox update to be able to run one single time.
            this.enabled = false;
        }
    }
}
