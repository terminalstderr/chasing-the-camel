﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpitBehaviour : MonoBehaviour {
	private bool get_key_down_spit;
	public GameObject DestroyableBlock;
	private CharacterAnimationBehavior m_animator;
    public AudioClip spit_sound;

    void Awake()
	{
		m_animator = GetComponent<CharacterAnimationBehavior> ();
	}
	void Update ()
	{

		if (Input.GetButtonUp("Spit")) 
		{
			Vector2 dir = m_animator.facing_right ? Vector2.right : Vector2.left;
			float xRounder = m_animator.facing_right ? -.5F : -.5F;
			GameObject destroyable_block = isSplitBlock (dir); 

			if (!destroyable_block && !isWall(dir))
			{
				Vector3 current = transform.position + (Vector3)dir;
				Vector3 snapGridVector = new Vector3 ((Mathf.Round (current.x)), Mathf.Round (current.y), Mathf.Round (current.z));
                SoundManager.instance.PlaySingle(spit_sound);
				Instantiate (DestroyableBlock, snapGridVector, Quaternion.identity);	
			} else 
			{
                SoundManager.instance.PlaySingle(spit_sound);
                Destroy(destroyable_block);
			}
		}
	}

	bool isWall(Vector2 direction)
	{
		int destroyable_layer = 1 << 8; //change 9 to an 8 to get it to a platform layer, additional check, check not only if hit, but also if the gameobject has the tag.
		RaycastHit2D rc_hit = Physics2D.Raycast(transform.position, direction, 1.0F, destroyable_layer);
		if (rc_hit && rc_hit.collider.gameObject.tag == "Untagged") {
			Debug.Log ("Raycast found wall object", gameObject);
			return true;
		}
		return false;
	}

	GameObject isSplitBlock(Vector2 direction)
	{
		int destroyable_layer = 1 << 8; //change 9 to an 8 to get it to a platform layer, additional check, check not only if hit, but also if the gameobject has the tag.
		RaycastHit2D rc_hit = Physics2D.Raycast(transform.position, direction, 1.0F, destroyable_layer);
		if (rc_hit && rc_hit.collider.gameObject.tag == "destroyable") {
			Debug.Log ("Raycast found destroyable object", gameObject);
			return rc_hit.transform.gameObject;
		}
		return null;
	}

}
