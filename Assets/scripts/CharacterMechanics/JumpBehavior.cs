﻿using UnityEngine;
using System.Collections;

public class JumpBehavior : MonoBehaviour {
    [Header("Jump Physics")]
    public float jump_height;
    public float jump_wall_force;
    public float ground_epsilon;
    public bool disable_walljump;

    [Header("Prolonged Jump Physics")]
    public int prolong_jump_frames;
    // 'exponent' 0 to 1
    public float b = 0.6f;
    // 'regardless' 0 to 1
    public float y = 0.8f;
    // 'multiplier' 1 to 20
    public float m = 10.0f;

    private bool get_key_down_jump = false;
    private bool is_jump_enabled = true;
    private Rigidbody2D m_rb;
    private Collider2D m_collider;

    private float distance_from_ground;
    private float distance_from_wall;


    // Use this for initialization
    void Awake() {
        m_rb = GetComponent<Rigidbody2D>();
        m_collider = GetComponent<Collider2D>();
        distance_from_ground = m_collider.bounds.extents.y;
        distance_from_wall = m_collider.bounds.extents.x;
    }

    // Get inputs during Update
    void Update() {
        get_key_down_jump = Input.GetButton("Jump");
    }

    // Do physics based operations during FixedUpdate
    void FixedUpdate()
    {
        if (get_key_down_jump && is_jump_enabled &&
            (isGrounded(Vector2.down) || 
            !disable_walljump && isGrounded(Vector2.right) || 
            !disable_walljump && isGrounded(Vector2.left)))
        {
            disable_jump_until_end_of_frame();
            StartCoroutine(prolong_jump());
            m_rb.velocity = new Vector2(m_rb.velocity.x, 0);
            // TODO -- Somehou
            Debug.Log(Time.realtimeSinceStartup + ": Jumping!");
            m_rb.AddForce(new Vector2(0.0F, jump_height));

            // If the wall is to our right, add a force to the left
            if (isGrounded(Vector2.right))
            {
                m_rb.velocity = new Vector2(0, m_rb.velocity.y);
                m_rb.AddForce(new Vector2(-jump_wall_force, 0.0F));
            }

            // If the wall is to our left, add a force to the right
            if (isGrounded(Vector2.left))
            {
                m_rb.velocity = new Vector2(0, m_rb.velocity.y);
                m_rb.AddForce(new Vector2(jump_wall_force, 0.0F));
            }
        }
    }

    private void disable_jump_until_end_of_frame()
    {
        is_jump_enabled = false;
        StartCoroutine(enable_jump());
    }

    private IEnumerator enable_jump()
    {
        int number_of_frames_to_disable_jump = 5;
        for (int i = 0; i < number_of_frames_to_disable_jump; i++)
            yield return new WaitForEndOfFrame();
        is_jump_enabled = true;
    }

    private IEnumerator prolong_jump()
    {
        for (int i = 0; i < prolong_jump_frames; i++)
        {
            if (get_key_down_jump)
            {
                m_rb.AddForce(-Physics2D.gravity * prolong_jump_coefficient(i));
            }
            yield return new WaitForFixedUpdate();
        }
    }

    private float prolong_jump_coefficient(int x)
    {
        return y + m * Mathf.Pow(b, x);
    }

    bool isGrounded()
    {
        return isGrounded(Vector2.down) || isGrounded(Vector2.left) || isGrounded(Vector2.right);
    }

    bool isGrounded(Vector2 direction)
    {
        int floor_layer = 1 << 8;
        bool ret = Physics2D.Raycast(transform.position, direction, distance_from_ground + ground_epsilon, floor_layer);
        if (ret)
            Debug.Log("Raycast down for ground check was true", gameObject);
        return ret;
    }

}
