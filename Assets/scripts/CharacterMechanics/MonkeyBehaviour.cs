﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonkeyBehaviour : ActivatedBehaviour {
	public float gravity_constant = .01f;
	private bool get_key_down_up;
	private Rigidbody2D m_rb;
	private bool disable_gravity;

	public override void Activate()
	{
		if (get_key_down_up) 
		{
			disable_gravity = true;
		}
	}

	public override void Deactivate()
	{
		disable_gravity = false;	
	}
		

	void Update ()
	{

        get_key_down_up = Input.GetAxis("Vertical") > 0 ? true : false;
        get_key_down_up = get_key_down_up || Input.GetButton("Up");
	}


	void FixedUpdate()
	{
		//Bug: Camel Accelerates when leaving ceiling.
		//Possible: AddForce is adding the force every time, use SetVelocity (But then no jump off functionality), but doesn't matter.
		if (disable_gravity) 
		{
			m_rb = GetComponent<Rigidbody2D>();
			m_rb.AddForce(-Physics2D.gravity + Vector2.up * gravity_constant);
			if (!get_key_down_up) 
			{
				disable_gravity = false;
			}
		}
	}
}
