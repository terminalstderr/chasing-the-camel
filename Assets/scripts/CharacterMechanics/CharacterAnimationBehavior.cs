﻿using UnityEngine;
using System.Collections;

public class CharacterAnimationBehavior : MonoBehaviour {
    private Rigidbody2D m_rb;
    private Animator m_animator;
    public bool facing_right = true;

    void Awake ()
    {
        m_rb = GetComponent<Rigidbody2D>();
        m_animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update () {
        float vspeed = m_rb.velocity.y < 0.2F ? 0.0F : m_rb.velocity.y;
        float hspeed = m_rb.velocity.x;
        m_animator.SetFloat("VerticalSpeed", vspeed);
        m_animator.SetFloat("HorizontalSpeed", Mathf.Abs(hspeed));

        // figure out if we have changed directions since last update
        if (hspeed > 0.1 && !facing_right ||
            hspeed < -0.1 && facing_right)
        {
            Flip();
        }
    }

    void AnimatorUpdate(float movement)
    {
    }

    void Flip()
    {
        // Borrowed from: https://unity3d.com/learn/tutorials/topics/2d-game-creation/creating-basic-platformer-game
        facing_right = !facing_right;
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }
}
