﻿using UnityEngine;
using System.Collections;

public class RunBehavior : MonoBehaviour {
    public float acceleration;// 10
    public float max_walk_speed;// 5
    public float max_sprint_speed;// 8

    private float get_axis_horizontal;
    private bool get_key_down_sprint;

    private Rigidbody2D m_rb;

    // Use this for initialization
    void Start () {
        m_rb = GetComponent<Rigidbody2D>();
    }

    // Get inputs during Update
    void Update ()
    {
        get_axis_horizontal = Input.GetAxis("Horizontal");
        get_key_down_sprint = Input.GetButton("Sprint");
    }

    // Do physics based operations during FixedUpdate
    void FixedUpdate()
    {
        float movement = get_axis_horizontal;
        m_rb.AddForce(new Vector2(movement * acceleration, 0.0F));
        float max_speed = get_key_down_sprint ? max_sprint_speed : max_walk_speed;
        m_rb.velocity = new Vector2(Mathf.Clamp(m_rb.velocity.x, -max_speed, max_speed), m_rb.velocity.y);
    }
}
