﻿using UnityEngine;
using System.Collections;
using System;
//TODO this is not as general as it should be. 2/16/17
public class VineActivator : MonoBehaviour {

	void OnCollisionEnter2D(Collision2D other)
	{
		MonkeyBehaviour mb = other.gameObject.GetComponent<MonkeyBehaviour>();
		if (mb) 
		{
			mb.Activate();
		}
	}

	void OnCollisionExit2D(Collision2D other)
	{
		MonkeyBehaviour mb = other.gameObject.GetComponent<MonkeyBehaviour>();
		if (mb) 
		{
			mb.Deactivate();
		}
	}

}
