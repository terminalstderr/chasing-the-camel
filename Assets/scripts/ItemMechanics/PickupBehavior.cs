﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//TODO this is not as general as it should be. 2/16/17
public class PickupBehavior : MonoBehaviour {

    public GameObject MonkeyBack;
    public AudioClip pickup_sound;

    void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.CompareTag ("Player")) 
		{
            SoundManager.instance.PlaySingle(pickup_sound);

            if (other.GetComponent<MonkeyBehaviour> () == null) 
			{
				other.gameObject.AddComponent<MonkeyBehaviour>();
                MonkeyBack.SetActive(true);
            }
			Destroy (this.gameObject);
		}

	}

}
