﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyActivatedBehaviour : MonoBehaviour {
    public GameObject activator;
    public GameObject door;
    private bool activated = false;

    void OnTriggerEnter2D(Collider2D other)
    {
        if (!activated && other.gameObject.CompareTag(activator.tag))
        { 
            activated = true;
            GetComponent<Animator>().SetTrigger("pickup");
            door.GetComponent<ActivatedBehaviour>().Activate();
            StartCoroutine(Utility.MoveOverSeconds(gameObject, door.transform.position, 2.0f));
            Destroy(gameObject, 2.0f);
        }
    }

}
