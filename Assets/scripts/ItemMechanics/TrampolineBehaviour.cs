﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrampolineBehaviour : MonoBehaviour {
    public AudioClip jump_sound;

	public float jump_multiplier;
	//If the player holds jump, jump behavior does not actually capture the multiplier.
	//Possibly: On collision exit is happening with the frame updates, while the jump behavior is happening with physics updates.
	void OnCollisionEnter2D(Collision2D other)
	{ 
		JumpBehavior jB = other.gameObject.GetComponent<JumpBehavior> ();
		if (jB != null) 
		{
			jB.jump_height *= jump_multiplier;
		}
	}

	void OnCollisionExit2D(Collision2D other)
	{
		JumpBehavior jB = other.gameObject.GetComponent<JumpBehavior> ();
		if (jB != null) 
		{
            jB.jump_height /= jump_multiplier;
            SoundManager.instance.PlaySingle(jump_sound);
        }
    }
}
 