﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorBehaviour : ActivatedBehaviour
{
    public AudioClip open_sound;
    private bool activated = false;

    void OnTriggerEnter2D(Collider2D other)
    {
        if (activated && other.gameObject.CompareTag("Player"))
        {
            GameManager.instance.level_finished();         
        }
    }

    public override void Activate()
    {
        if (!activated)
        {
            activated = true;
            SoundManager.instance.PlaySingle(open_sound);
            GetComponent<Animator>().SetTrigger("open");
        }
    }

}
